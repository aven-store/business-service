package com.avenstore.businessservice;

import com.avenstore.businessservice.model.Application;
import com.avenstore.businessservice.utils.CustomPage;
import com.nimbusds.jwt.JWT;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.event.annotation.BeforeTestMethod;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@SpringBootTest
@ContextConfiguration(classes = BusinessServiceApplication.class)
@AutoConfigureWebTestClient
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
public class ApplicationIntegrationTests {
    static final long EXPIRATIONTIME = 864_000_000; // 10 days
    static final String SECRET = "ThisIsASecret";
    static final String TOKEN_PREFIX = "Bearer";
    static final String HEADER_STRING = "Authorization";
    @Autowired
    WebTestClient webTestClient;
    Application application;
    @BeforeEach
    public void prepare(){
        application=new Application();
        application.setBusinessName("test");
        application.setEmail("test@test.com");
        application.setAddress("rue test");
        application.setCountryCode("SN");
        application.setDescription("test d'integration");
        application.setPhoneNumber1(63986532);
        application.setDomainTags(Set.of("TELEPHONIE"));

    }
//    @Test
//    @WithMockUser(username = "test")
//    @Order(1)
//    public void applyTest(){
//        webTestClient.mutateWith(csrf()).post().uri("/aven-store/business-service/apply")
//                .bodyValue(application)
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus()
//                .isCreated()
//                .expectBody(Application.class)
//        .consumeWith(rs->{
//            Assertions.assertEquals(rs.getResponseBody().getBusinessName(),"test");
//        })
//        ;
//    }
    @Test
    @WithMockUser(username = "test")
    @Order(2)
    public void applyTestWithInvalidData(){
        application.setBusinessName(null);
        webTestClient.mutateWith(csrf()).post().uri("/aven-store/business-service/apply")
                .bodyValue(application)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isBadRequest();
    }
    @Test
    @WithMockUser(username = "test")
    @Order(3)
    public void applyTestWithNullBody(){
        webTestClient.mutateWith(csrf()).post().uri("/aven-store/business-service/apply")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isBadRequest();
    }
    @Test
    @WithMockUser(username = "test")
    @Order(4)
    public void findAllTest(){
        webTestClient.get().uri("/aven-store/business-service/applications")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(CustomPage.class)
                .consumeWith(rs->{
                   Assert.isTrue(!rs.getResponseBody().getContent().isEmpty(),"");
                })
        ;
    }
    @Test
    @WithMockUser(username = "test")
    @Order(5)
    public void findAllTestWithFilter(){
        webTestClient.get().uri("/aven-store/business-service/applications?filterBy=businessName&value=test")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(CustomPage.class)
                .consumeWith(rs->{
                    Assert.isTrue(!rs.getResponseBody().getContent().isEmpty(),"");
                })
        ;
    }
    @Test
    @WithMockUser(username = "test")
    @Order(6)
    public void findAllTestWithLimit(){
        webTestClient.get().uri("/aven-store/business-service/applications?limit=0")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isBadRequest()
        ;
    }
    public static String createToken(String username) {
        String jwt = Jwt.withTokenValue("token").expiresAt(new Date(System.currentTimeMillis() + EXPIRATIONTIME).toInstant())
                .subject(username).toString();

        return jwt;
    }
}
