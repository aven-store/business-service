package com.avenstore.businessservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This class define the state of a business or it's stores.It's used for example to verify whether
 * the business is available to the public or whether it's blocked for some reason.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class State {
    /**
     * Determine if the business is activated and available to the public.
     */
    private boolean activated;
    /**
     * Determines if the business is removed
     */
    private boolean removed;
    /**
     * determine if the business is blocked by the administration.
     */
    private boolean blocked;
}
