package com.avenstore.businessservice.model;

import lombok.AllArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.time.DayOfWeek;
@AllArgsConstructor
/**
 * This class define information of typical work day for a store.
 *
 */
public class WorkDay {
    /**
     * The corresponding day of week
     */
    private final DayOfWeek day;
    /**
     * The time from wish the store is opened.
     */
    private final Time openAt;
    /**
     * The time from wish the store is closed.
     */
    private final Time closeAt;

    public WorkDay setDay(@NotNull DayOfWeek day){
        return new WorkDay(day,this.openAt,this.closeAt);
    }
    public WorkDay setTimes(@NotNull Time openingTime,@NotNull Time closingTime){
        return new WorkDay(day,openingTime,closingTime);
    }
}
