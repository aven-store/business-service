package com.avenstore.businessservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.sql.Time;
import java.time.DayOfWeek;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

/**
 * a store represent a physical commerce that belong to a business.
 */
@NoArgsConstructor
@Document
@TypeAlias("store")
public class Store {
    /**
     * Identification of the store.
     */
    @Id
    private String id;
    /**
     * Id of the business corresponding to the store.
     */
    private String businessId;
    /**
     * The name of the store
     */
    private String label;
    /**
     * The location address of the store.
     */
    private String address;
    private Long phoneNumber1;
    private Long phoneNumber2;
    /**
     * The region where the store is located.
     */
    private String region;
    /**
     * Define whether the store is opened or closed;
     */
    private boolean opened;
    /**
     * Define if the store is the default store created with the business.
     */
    private boolean isDefault;
    @CreatedDate
    private Date createdAt;
    private Date deletedAt;
    private Set<WorkDay> workDays;
    private State state;
    private Set<Category> categories;
    @CreatedBy
    private String author;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Store)) return false;
        Store store = (Store) o;
        return opened == store.opened &&
                isDefault == store.isDefault &&
                id.equals(store.id) &&
                Objects.equals(businessId, store.businessId) &&
                Objects.equals(label, store.label) &&
                Objects.equals(address, store.address) &&
                Objects.equals(phoneNumber1, store.phoneNumber1) &&
                Objects.equals(phoneNumber2, store.phoneNumber2) &&
                Objects.equals(region, store.region) &&
                Objects.equals(createdAt, store.createdAt) &&
                Objects.equals(deletedAt, store.deletedAt) &&
                Objects.equals(workDays, store.workDays) &&
                Objects.equals(state, store.state) &&
                Objects.equals(categories, store.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, businessId, label, address, phoneNumber1, phoneNumber2, region, opened, isDefault, createdAt, deletedAt, workDays, state, categories);
    }
}

