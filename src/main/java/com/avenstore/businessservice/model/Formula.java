package com.avenstore.businessservice.model;

/**
 * The formula define the scope of the business.
 * Standard: this formula is the default one it provide a limited set of feature for the business.
 * Premium: it grants access to all features available for businesses.
 */
public enum Formula {
    PREMIUM,STANDARD
}
