package com.avenstore.businessservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * this class is used to store information about a store.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusinessStoreMapping {
    private Store storeId;
    /**
     * this information indicate whether the store is a default one or note.
     */
    private boolean isDefault;
}
