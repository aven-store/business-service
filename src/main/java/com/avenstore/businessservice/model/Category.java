package com.avenstore.businessservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.Set;
@AllArgsConstructor
@Data
/**
 * A category define set of products that are similar.
 */
public class Category {
    /**
     * the label is a unique name of a category.
     */
    private final String label;
    /**
     * the tag collection contain a list of keywords that defines the category
     */
    private final Set<String> tags;

    /**
     * this method is used to add tags to the category
     * @param tags list of tags to add to the category's tag
     * @return A Category object
     */
    public Category addTags(String ...tags){
        for (String tag:tags) {
            if (!tag.isEmpty()){
                this.tags.add(tag);
            }
        }
       return new Category(label,this.tags);
    }
    /**
     * this method is used to remove tags from the category
     * @param tags list of tags to remove from the category's tag
     * @return  Category
     */
    public Category removeTags(String ...tags){
        this.tags.removeAll(Set.of(tags));
        return new Category(this.label,this.tags);
    }
}
