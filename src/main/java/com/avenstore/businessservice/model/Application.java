package com.avenstore.businessservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.domain.Persistable;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Document
@TypeAlias("applications")
@Data
@NoArgsConstructor
/**
 * An application an object that represent a demand made by a user in order to create a business.
 */
@NotNull
public class Application implements Serializable {
    @Id
    String id;
    @NotNull
    @NotEmpty
    private String address;


    @NotNull
    @NotEmpty
    private String businessName;


    /**
     * The official email address of a business
     */
    @NotNull
    @NotEmpty
    private String email;


    /**
     * The first official phone number of a business
     */

    private long phoneNumber1;


    /**
     * The second official phone number of a business
     */
    private long phoneNumber2;

    @CreatedDate
    Date applicationDate;


    /**
     * Id of user who submit the application
     */
    @CreatedBy
    String userId;

    @NotNull
    @NotEmpty
    @Size(max = 5)
    String countryCode;

    /**
     * key words that defines the products types that the owner wants to sell
     */
    @NotNull
    @NotEmpty
    private Set<String> domainTags;
    /**
     * A short description of what the business is about and what are the needs.
     */
    private String description;
}
