package com.avenstore.businessservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@Document
@TypeAlias("business")
@Data
public class Business {
    /**
     * The id is a unique identification value for avery Business
     */
    @Id
    private String id;
    /**
     * The address that locate the business
     */
    private String address;
    /**
     * The official email address of a business
     */
    private String email;
    /**
     * The first official phone number of a business
     */
    private long phoneNumber1;
    /**
     * The second official phone number of a business
     */
    private long phoneNumber2;
    /**
     * An url to the business online store
     */
    private String url;
    /**
     * The date of the business creation
     */
    @CreatedDate
    private Date createdAt;
    /**
     * This is the business's author id
     */
    @CreatedBy
    private String author;
    /**
     * The country in wish the business is located
     */
    private String country;
    /**
     * List of categories that defines products of the business
     */
    private Set<Category> category;
    /**
     * It defines the type of business(SARL,ALS...).
     */
    private String corporateName;

    private Formula formula;

    private State state;

    private List<BusinessStoreMapping> stores;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Business)) return false;
        Business business = (Business) o;
        return phoneNumber1 == business.phoneNumber1 &&
                phoneNumber2 == business.phoneNumber2 &&
                id.equals(business.id) &&
                Objects.equals(address, business.address) &&
                Objects.equals(email, business.email) &&
                Objects.equals(url, business.url) &&
                Objects.equals(createdAt, business.createdAt) &&
                Objects.equals(country, business.country) &&
                Objects.equals(category, business.category) &&
                Objects.equals(corporateName, business.corporateName) &&
                formula == business.formula &&
                Objects.equals(state, business.state) &&
                Objects.equals(stores, business.stores);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, address, email, phoneNumber1, phoneNumber2, url, createdAt, country, category, corporateName, formula, state, stores);
    }
}
