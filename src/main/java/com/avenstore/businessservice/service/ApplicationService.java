package com.avenstore.businessservice.service;

import com.avenstore.businessservice.exceptions.DataSavingException;
import com.avenstore.businessservice.exceptions.config.DomainException;
import com.avenstore.businessservice.model.Application;
import com.avenstore.businessservice.repository.ApplicationRepository;
import com.avenstore.businessservice.utils.CustomPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import java.awt.print.Pageable;

@Service
public class ApplicationService {
    private ApplicationRepository repository;
    private EventService eventService;
    @Autowired
    public ApplicationService(ApplicationRepository repository,EventService eventService){
        this.repository=repository;
        this.eventService=eventService;
    }
    public Mono<Application> createApplication(@NotNull Application application){
        return repository.save(application)
                .map(app->{
                    eventService.sendApplicationSentEvent(app);
                    return app;
                })
                .onErrorMap(throwable ->
                        new DataSavingException("Enable to save the application",
                                throwable.getStackTrace())
                );
    }

    public Mono<CustomPage<Application>> findAll(int offset, int limit, Sort.Direction direction, @Nullable String filter, @Nullable String value,String[] sortBy){
        CustomPage<Application> page=new CustomPage<>();
        return repository.count()
                .map(count->{
                 page.setCount(count);
                 return count;
                })
                .flatMapMany(c->{
                    if (filter==null||value==null){
                        return repository.getAllWithLimit( PageRequest.of(offset,limit,Sort.by(direction,sortBy)));
                    }
                    return repository.getAllWithLimit(PageRequest.of(offset,limit,Sort.by(direction,sortBy)),filter,value);
                })
                .collectList()
                .map(apps->{
                    page.setContent(apps);
                    page.setLimit(limit);
                    page.setOffset(offset);
                    page.setCount(apps.size());
                    return page;
                })
                .onErrorMap(throwable ->{
                    throwable.printStackTrace();
                    return new DomainException("impossible to find application something goes wrong", throwable.getStackTrace());
                } );
    }
}
