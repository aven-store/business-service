package com.avenstore.businessservice.config.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Optional;
@Component
public class UserAudit implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
       Mono<SecurityContext> securityContext=ReactiveSecurityContextHolder.getContext();
        return securityContext.map(context->context.getAuthentication().getPrincipal().toString()).blockOptional();
    }

}
