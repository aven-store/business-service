package com.avenstore.businessservice.utils;

import lombok.Data;

import java.util.Collection;

@Data
public class CustomPage<T> {
    private  long count;
    private  long offset;
    private long limit;
    private  Collection<T> content;

}
