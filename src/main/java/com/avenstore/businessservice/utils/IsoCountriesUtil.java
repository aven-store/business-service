package com.avenstore.businessservice.utils;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class IsoCountriesUtil {
    private static final Set<String> ISO_COUNTRIES_CODE= new HashSet<>(Arrays.asList(Locale.getISOCountries()));
    public static boolean isValidCountryCode(String code){
        return ISO_COUNTRIES_CODE.contains(code);
    }
}
