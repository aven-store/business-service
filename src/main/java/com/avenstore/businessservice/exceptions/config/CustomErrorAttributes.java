package com.avenstore.businessservice.exceptions.config;

import com.avenstore.businessservice.exceptions.DataSavingException;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Map;
@Component
public class CustomErrorAttributes extends DefaultErrorAttributes {
    private final String DATASAVINGEXCEPTION="com.avenstore.businessservice.exceptions.DataSavingException";
    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        Map<String,Object> map=super.getErrorAttributes(request,options);
        Throwable error=getError(request);
        switch (error.getClass().getName()){
            case DATASAVINGEXCEPTION:{
                map.put("message",error.getMessage());
                map.put("error","Server Error");
                map.put("status", 500);
                map.put("stackTrace",error.getStackTrace());
                return map;
            }
            default:{
                map.put("message","Error");
                map.put("error","Server Error");
                map.put("stackTrace",error.getStackTrace());
                map.put("status", 500);
                return map;
            }
        }
    }
}
