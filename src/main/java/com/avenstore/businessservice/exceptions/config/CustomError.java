package com.avenstore.businessservice.exceptions.config;

import lombok.Data;

import java.io.Serializable;
@Data
public class CustomError implements Serializable {
    private String message;
    private int status;
    private String error;

    public CustomError() {
    }

    public CustomError(String message, int status, String error) {
        this.message = message;
        this.status = status;
        this.error = error;
    }
}
