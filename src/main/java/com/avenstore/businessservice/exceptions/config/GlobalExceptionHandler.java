package com.avenstore.businessservice.exceptions.config;

import com.avenstore.businessservice.exceptions.DataSavingException;
import com.avenstore.businessservice.exceptions.InvalidDataException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;

import java.io.Serializable;

@ControllerAdvice

public class GlobalExceptionHandler {
    /**
     * this Handler is for handling exception when Entity saving operation end up with error
     * @param ex
     * @return ResponseEntity with 500 error code
     */
    @ExceptionHandler(DataSavingException.class)
    public ResponseEntity<CustomError> dataSavingExceptionHandler(DataSavingException ex){
        return new ResponseEntity(
                new CustomError(
                      ex.getMessage(),
                      500,
                        HttpStatus.INTERNAL_SERVER_ERROR.toString()
                ),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    /**
     * This handler occurs when user request body contain an invalid data
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {InvalidDataException.class, WebExchangeBindException.class})
    public ResponseEntity<CustomError> invalidDataExceptionHandler(RuntimeException ex){
        return new ResponseEntity(
                new CustomError(
                        "Invalid data",
                        400,
                        HttpStatus.BAD_REQUEST.toString()
                ),
                HttpStatus.BAD_REQUEST
        );
    }

}
