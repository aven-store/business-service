package com.avenstore.businessservice.exceptions;

import com.avenstore.businessservice.exceptions.config.DomainException;

public class DataSavingException extends DomainException {

    public DataSavingException(String message,StackTraceElement[] log){
        super(message,log);
    }
}
