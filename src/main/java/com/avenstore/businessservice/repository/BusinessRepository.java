package com.avenstore.businessservice.repository;

import com.avenstore.businessservice.model.Business;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface BusinessRepository extends ReactiveMongoRepository<Business,String> {
}
