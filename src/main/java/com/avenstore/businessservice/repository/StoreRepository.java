package com.avenstore.businessservice.repository;

import com.avenstore.businessservice.model.Store;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface StoreRepository extends ReactiveMongoRepository<Store,String> {

}
