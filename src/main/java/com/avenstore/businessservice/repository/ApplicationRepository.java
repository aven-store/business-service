package com.avenstore.businessservice.repository;

import com.avenstore.businessservice.model.Application;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;
import reactor.core.publisher.Flux;


@Repository
public interface ApplicationRepository extends ReactiveMongoRepository<Application, String> {
    @Query(value = "{_id:{$exists:true}}")
    Flux<Application> getAllWithLimit(Pageable pageable);
    @Query(value = "{?0:?1}")
    Flux<Application> getAllWithLimit(Pageable pageable,String filterBy,String value);
}
