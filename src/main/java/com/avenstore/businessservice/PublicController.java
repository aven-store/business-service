package com.avenstore.businessservice;

import com.avenstore.businessservice.exceptions.InvalidDataException;
import com.avenstore.businessservice.model.Application;
import com.avenstore.businessservice.service.ApplicationService;
import com.avenstore.businessservice.utils.CustomPage;
import com.avenstore.businessservice.utils.IsoCountriesUtil;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.lang.reflect.Array;
import java.net.URI;

@RestController
@RequestMapping("aven-store/business-service/")
public class PublicController {
    private final ApplicationService applicationService;
    public PublicController(ApplicationService appService){
        this.applicationService=appService;
    }


    @PostMapping(path = "apply",consumes = "application/json")
    public Mono<ResponseEntity<Application>> apply(@RequestBody(required = false) @Valid Application application, @AuthenticationPrincipal Jwt auth){
        if (application==null){
            throw new InvalidDataException("No application data found ");
        }
        if (IsoCountriesUtil.isValidCountryCode(application.getCountryCode())){
            application.setUserId(auth.getId());
       return
               applicationService.createApplication(application)
               .map(app-> ResponseEntity
                       .created(URI.create("aven-store/business-service/apply/"+app.getId()))
                       .body(app));
        }
        else throw new InvalidDataException("invalid country code");
    }

    @GetMapping(path = "applications",produces = "application/json")
    public Mono<CustomPage<Application>> findAll(
            @RequestParam(required = false) Integer limit,
            @RequestParam(required = false) Integer offset,
            @RequestParam(required = false) String filterBy,
            @RequestParam(required = false) String value,
            @RequestParam(required = false) String[] sortBy,
            @RequestParam(required = false) String direction
    ){
        if (limit!=null&&limit ==0){
            throw new InvalidDataException("Limit must not be null!!");
        }
        return applicationService.findAll(
                offset!=null?offset:0,
                limit!=null?limit: Integer.MAX_VALUE,
                direction!=null&&direction.equalsIgnoreCase("desc")? Sort.Direction.DESC: Sort.Direction.ASC,
                filterBy!=null&&filterBy.isEmpty()?null:filterBy,
                value!=null&&value.isEmpty()?null:value,
                sortBy!=null?sortBy: new String[]{"businessName"}
        );
    }
}
